let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

/*
* My own style
* */
mix.styles([
    'resources/assets/css/bootstrap.min.css',
    'resources/assets/css/bootstrap-social.css',
    'resources/assets/css/metisMenu.min.css',
    'resources/assets/css/startmin.css',
    'resources/assets/css/font-awesome.min.css'
], 'public/admin/css/all.css');

/*
* My own js files
* */
mix.js([
    'resources/assets/js/bootstrap.js',
    'resources/assets/js/jquery.min.js',
    'resources/assets/js/metisMenu.min.js',
    'resources/assets/js/startmin.js',
], 'public/admin/js/all.js');