@extends('layouts.admin')
@section('content')
    {{-- Model form binding --}}
    {!! Form::model($tag,['action' => ['TagController@update',  $tag->id]]) !!}
    {{ method_field('PATCH') }}
    <div class="form-group @if($errors->has('name')) has-error @endif">
        {!! Form::label('name', 'name'); !!}
        {!! Form::text('name',null, ['id' => 'title', 'class' =>'form-control']); !!}
        @if($errors->has('name')) <p class="text-danger">{{ $errors->first('name') }}</p> @endif
    </div>
    <div class="col-lg-4">
        {!! Form::submit('Edit' ,['class' => 'btn btn-success']); !!}
    </div>
    {!! Form::close() !!}

    <div class="col-md-3">
        <a href="{{ URL::previous() }}" class="btn btn-lg btn-link btn-primary">Back</a>
    </div>

@endsection