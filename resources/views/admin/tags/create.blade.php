@extends('layouts.admin')
@section('title' , 'New Tag')
@section('content')
    {!! Form::open(['method' => 'POST','action' => 'TagController@store']) !!}
    {{-- Display validation errors for each field  --}}
    <div class="form-group @if($errors->has('name')) has-error @endif">
        {!! Form::label('name', 'Name'); !!}
        {!! Form::text('name','', ['id' => 'name', 'class' =>'form-control']); !!}
        @if($errors->has('name')) <p class="text-danger">{{ $errors->first('name') }}</p> @endif
    </div>
    {!! Form::submit('Create' ,['class' => 'btn btn-primary']); !!}
    {!! Form::close() !!}
@endsection
