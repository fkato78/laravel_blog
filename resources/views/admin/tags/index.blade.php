@extends('layouts.admin')
@section('title' , 'Tags')
@section('content')
    <a href="{{ route('tags.create') }}" class="btn btn-primary">Create new tag</a>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @if($tags->count() == 0)
                <tr><td><h1>There is no content</h1></td></tr>
            @else
            @foreach($tags as $tag)
                <tr>
                    <td>{{ $tag->id }}</td>
                    <td>{{$tag->name}}</td>
                    <td>{!! Form::open(['method' => 'DELETE', 'action' => ['TagController@destroy', $tag->id]]) !!}
                        <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                        {!! Form::close() !!}
                    </td>
                    <td><a href="{!! action('TagController@edit',  $tag->id) !!}" class="btn btn-success"><i class="fa fa-check-circle"></i></a></td>
                </tr>
            @endforeach
             @endif
            </tbody>
        </table>
    </div>
    {{-- Pagination links --}}
{{ $tags->links() }}
@endsection



