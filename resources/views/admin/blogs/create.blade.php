@extends('layouts.admin')
@section('title' , 'New post')
@section('content')
{{-- Tinymce editor--}}
@include('partials._tinymce_editor')
    {!! Form::open(['method' => 'POST','action' => 'BlogController@store', 'files' => true]) !!}
    {{-- Display validation errors for each field  --}}
    <div class="form-group @if($errors->has('title')) has-error @endif">
    {!! Form::label('title', 'Title'); !!}
    {!! Form::text('title','', ['id' => 'title', 'class' =>'form-control']); !!}
        @if($errors->has('title')) <p class="text-danger">{{ $errors->first('title') }}</p> @endif
    </div>
    {!! Form::label('body', 'body'); !!}
    {{--{!! Form::textarea('body','', ['id' => 'body', 'class' =>'form-control my-editor']); !!}--}}
    <textarea name="body" class="form-control my-editor" id="body">{!! old('body') !!}</textarea>
    <div class="form-group">
        <label>Categories</label><br >
        @foreach($categories as $category)
            <input type="checkbox" name="category_id[]" value="{{ $category->id }}">
            <label class="checkbox-inline">{{ $category->name }}</label>
        @endforeach
    </div>
    <div class="form-group">
        <label>Tags</label>
        <select name="tag_id[]" multiple class="form-control">
            @foreach($tags as $tag)
                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
            @endforeach
        </select>
    </div>
<button type="reset" class="btn btn-default">Reset</button>

    <div class="form-group">
        <label>Featured image</label>
        <input type="file" name="featured_image" class="form-control">
    </div>

    {!! Form::submit('Create' ,['class' => 'btn btn-primary']); !!}
    {!! Form::close() !!}
@endsection
