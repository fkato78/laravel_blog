@extends('layouts.admin')
@section('content')
@include('partials._tinymce_editor')
    {{-- Model form binding --}}
    {!! Form::model($blog,['action' => ['BlogController@update',  $blog->id],'files' => true]) !!}
    {{ method_field('PATCH') }}
    {!! Form::label('title', 'Title'); !!}
    {!! Form::text('title',null, ['id' => 'title', 'class' =>'form-control']); !!}
    {!! Form::label('body', 'body'); !!}
    {{--{!! Form::textarea('body',null, ['id' => 'body', 'class' =>'form-control']); !!}--}}
    <textarea name="body" class="form-control my-editor" id="body">{!! old('body', $blog->body) !!}</textarea>
    <div class="form-group">
        <label>Categories : </label>
        @foreach($allCategories as $category)
            <input type="checkbox" name="category_id[]" value="{{ $category->id }}"
            @if($blog->categories->count() > 0)
                @foreach($blog->categories as $blogCategory)
                    @if($blogCategory->id === $category->id)
                        checked
                    @endif
                @endforeach
            @endif
            >
            <label class="checkbox-inline">{{ $category->name }}</label>
        @endforeach

    </div>
    {{--@if($blog->tags->count() > 0)--}}
        {{--<div class="form-group">--}}
            {{--<label>Tags</label>--}}
            {{--@foreach($allTags as $tag)--}}
            {{--<input type="checkbox" name="tag_id[]" value="{{ $tag->id }}"--}}
               {{--@foreach($blog->tags as $blogTag) checked @endforeach--}}
            {{-->--}}
                {{--<label class="checkbox-inline">{{ $tag->name }}</label>--}}
            {{--@endforeach--}}
        {{--</div>--}}
    {{--@endif--}}

    <div class="form-group">
        <div class="col-md-12">
            <div class="col-md-9">
                <label>Featured image</label>
                <input type="file" name="featured_image" class="form-control">
            </div>
            <div class="col-md-3">
                @if($blog->featured_image)
                    <img width="100" src="/uploads/featured_images/{{ $blog->featured_image }}" alt="{{ $blog->title }}">
                @else
                    <img width="100" src="/uploads/featured_images/no_image.jpg" alt="{{ $blog->title }}">
                @endif
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Form::submit('Edit' ,['class' => 'btn btn-success']); !!}
    </div>
    {!! Form::close() !!}

    <div class="form-group">
        <a href="{{ URL::previous() }}" class="btn btn-lg btn-link btn-primary">Back</a>
    </div>

@endsection