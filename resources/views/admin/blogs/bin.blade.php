@extends('layouts.admin')
@section('title' , 'Trashed posts')
@section('content')
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>id</th>
                <th>title</th>
                <th>body</th>
                <th>actions</th>
            </tr>
            </thead>
            <tbody>
            @if($deletedBlogs->count() == 0)
                <tr><td><h1>There is no content</h1></td></tr>
            @else
            @foreach($deletedBlogs as $blog)
                <tr>
                    <td>{{ $blog->id }}</td>
                    <td>{{$blog->title}}</td>
                    <td><p>{{str_limit($blog->body, 100, '...')}}</p></td>
                    <td>{!! Form::open(['method' => 'DELETE', 'action' => ['BlogController@removePermanent', $blog->id]]) !!}
                        <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                        {!! Form::close() !!}
                    </td>
                    <td><a href="{!! action('BlogController@restore', ['id' => $blog->id]) !!}" class="btn btn-success"><i class="fa fa-check-circle"></i></a></td>
                </tr>
            @endforeach
             @endif
            </tbody>
        </table>
    </div>
@endsection



