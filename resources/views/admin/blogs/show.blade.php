@extends('layouts.admin')

@section('title' , $blog->title)
@section('content')
    <div class="container-fluid">
        <div class="col-md-12">
            @if($blog->featured_image)
                <img class="img-responsive featured-img" src="/uploads/featured_images/{{ $blog->featured_image ?? "" }}" alt="{{ str_limit($blog->title, 50) }}">
            @endif    
        </div>
    </div>
    <p><b>Created by: {{ $blog->user->name }}</b></p>
    <p>{!! $blog->body !!}</p>
    <div class="col-md-12">
        <a href="{{ route('blogs.index') }}" class="btn btn-lg btn-link btn-primary">Back</a>
        <br>
        <strong>Categories : </strong>
        @foreach($blog->categories as $category)
        <a href="{{ route('categories.show', $category->slug) }}">{{ $category->name }}</a>
        @endforeach
        <br>
        <strong>Tags : </strong>
        <ul class="">
            @foreach($blog->tags as $tag)
                <li class="list-item"> {{ $tag->name }}</li>
            @endforeach
        </ul>


    </div>
@endsection