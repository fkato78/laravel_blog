@extends('layouts.admin')
@section('title' , 'Posts')
@section('content')
    {{--<div class="col-lg-12">--}}
        <a href="{{ route('blogs.create') }}" class="btn btn-primary">Create new blog</a>
    {{--</div>--}}
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Featured image</th>
                <th>Title</th>
                <th>Body</th>
                <th>Categories</th>
                <th>tags</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @if($blogs->count() == 0)
                <tr><td><h1>There is no content</h1></td></tr>
            @else
            @foreach($blogs as $blog)
                <tr>
                    <td>
                        @if($blog->featured_image)
                            <img  src="/uploads/thumbnails/{{ $blog->thumbnail }}" alt="{{ $blog->title }}">
                         @else
                            <img width="100" src="/uploads/featured_images/no_image.jpg" alt="{{ $blog->title }}">
                        @endif    
                    </td>
                    <td>{{$blog->title}}</td>
                    <td><p>{!! str_limit($blog->body, 50, '...')!!}</p></td>
                    <td>
                        @if($blog->categories->count() > 0)
                        @foreach($blog->categories as $category)
                            <a href="{{ route('categories.show', $category->slug) }}">{{ $category->name }}</a> &nbsp;
                        @endforeach
                        @else
                            <strong>Not categorized</strong>
                        @endif
                    </td>
                    <td>
                        @if($blog->tags->count() > 0)
                            @foreach($blog->tags as $tag)
                                {{--{{ route('categories.show', $category->slug) }}--}}
                                {{ $tag->name }}
                            @endforeach
                        @else
                            <strong>No tags</strong>
                        @endif
                    </td>
                    <td>
                        @if($blog->status !=true)
                            <a href="{{ route('blogs.change_status', $blog->id) }}" class="btn btn-danger btn-xs">pending</a>
                        @else
                            <a href="{{ route('blogs.change_status', $blog->id) }}" class="btn btn-success btn-xs" >published</a>
                        @endif
                    </td>
                    <td>{!! Form::open(['method' => 'DELETE', 'action' => ['BlogController@destroy', $blog->id]]) !!}
                        <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                        {!! Form::close() !!}
                    </td>
                    <td><a href="{!! action('BlogController@edit', ['id' => $blog->id]) !!}" class="btn btn-success"><i class="fa fa-check-circle"></i></a></td>
                    <td><a href="{!! action('BlogController@show', $blog->id) !!}" class="btn btn-primary"><i class="fa fa-eye"></i></a></td>
                </tr>
            @endforeach
             @endif
            </tbody>
        </table>
    </div>
    {{-- Pagination links --}}
{{ $blogs->links() }}
@endsection



