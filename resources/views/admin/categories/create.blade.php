@extends('layouts.admin')
@section('title' , 'New category')
@section('content')
{{--  Tiny editor  --}}
@include('partials._tinymce_editor')
    <form action="{{ action('CategoryController@store') }}" method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="form-group">
            <label for="body">Body</label>
            <textarea name="body" id="body" cols="30" rows="10" class="form-control my-editor"></textarea>
        </div>
        <div class="form-group">
            <label for="cat_image">Image</label>
            <input type="file" class="form-control" id="cat_image" name="cat_image">
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
        {{ csrf_field() }}
    </form>


@endsection