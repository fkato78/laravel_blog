@extends('layouts.admin')
@section('title' , $category->name)
@section('content')
    <div class="col-xs-12">
        <img src="/uploads/category_images/{{ $category->cat_image }}" alt="{{ $category->name }}" class="img-responsive">
    </div>
    <div class="col-xs-12">
        {!! $category->body !!}
    </div>
    <div class="col-md-3">
        <a href="{{ URL::previous() }}" class="btn btn-lg  btn-primary">Back</a>
        <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-lg b btn-warning">Edit</a>
    </div>

@endsection