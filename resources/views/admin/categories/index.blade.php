@extends('layouts.admin')
@section('title' , 'Categories')
@section('content')
    <a href="{{ route('categories.create') }}" class="btn btn-primary">Create new category</a>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>id</th>
                <th>slug</th>
                <th>name</th>
                <th>actions</th>
            </tr>
            </thead>
            <tbody>
            @if($categories->count() == 0)
                <tr><td><h1>There is no content</h1></td></tr>
            @else
            @foreach($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{$category->slug}}</td>
                    <td>{{$category->name}}</td>
                    <td>{!! Form::open(['method' => 'DELETE', 'action' => ['CategoryController@destroy', $category->id]]) !!}
                        <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                        {!! Form::close() !!}
                    </td>
                    <td><a href="{!! action('CategoryController@edit',  $category->id) !!}" class="btn btn-success"><i class="fa fa-check-circle"></i></a></td>
                    <td><a href="{!! action('CategoryController@show', $category->slug) !!}" class="btn btn-primary"><i class="fa fa-eye"></i></a></td>
                </tr>
            @endforeach
             @endif
            </tbody>
        </table>
    </div>
    {{-- Pagination links --}}
{{--{{ $categories->links() }}--}}
@endsection



