@extends('layouts.admin')
@section('content')
    {{--Tiny editor --}}
    @include('partials._tinymce_editor')
    {{-- Model form binding --}}
    {!! Form::model($category,['action' => ['CategoryController@update',  $category->id], 'files' => true]) !!}
    {{ method_field('PATCH') }}
    {!! Form::label('name', 'name'); !!}
    {!! Form::text('name',null, ['id' => 'title', 'class' =>'form-control']); !!}
    <textarea name="body" class="form-control my-editor" id="body">{!! old('body', $category->body) !!}</textarea>
    <div class="form-group">
        <div class="col-md-12">
            <div class="col-md-9">
                <label>Category image</label>
                <input type="file" name="cat_image" class="form-control">
            </div>
            <div class="col-md-3">
                @if($category->cat_image)
                    <img width="100" src="/uploads/category_images/{{ $category->cat_image }}" alt="{{ $category->title }}">
                @else
                    <img width="100" src="/uploads/featured_images/no_image.jpg" alt="{{ $category->title }}">
                @endif
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        {!! Form::submit('Edit' ,['class' => 'btn btn-success']); !!}
    </div>
    {!! Form::close() !!}

    <div class="col-md-3">
        <a href="{{ URL::previous() }}" class="btn btn-lg btn-link btn-primary">Back</a>
    </div>

@endsection