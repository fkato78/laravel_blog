@include('partials._header')
<!-- Navigation -->
@include('partials._nav')
{{-- Page content --}}
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-md-9">
                    <h1 class="page-header">@yield('title', 'Dashboard')</h1>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            @include('partials._messages')
            <div class="col-lg-12">
                @yield('content')
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->



@include('partials._footer')