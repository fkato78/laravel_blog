<div class="col-lg-12">
{{-- no category message : this will display when user try to create post BUT there are no category --}}
@if (Session::has('noCategory'))
    <div class="alert alert-info">{{ Session::get('noCategory') }}</div>
@endif

{{-- success messages --}}
@if (Session::has('success'))
    <div class="alert alert-success">{{ Session::get('success') }}</div>
@endif
</div>

