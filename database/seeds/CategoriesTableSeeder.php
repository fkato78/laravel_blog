<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $php = new \App\Category();
        $php->name = 'PHP';
        $php->slug = 'php';
        $php->save();

        $Ruby = new \App\Category();
        $Ruby->name = 'RUBY';
        $Ruby->slug = 'Ruby';
        $Ruby->save();

        $python = new \App\Category();
        $python->name = 'PYTHON';
        $python->slug = 'python';
        $python->save();
    }
}
