<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name'];
    // Many to many with blogs
    public function blogs()
    {
        return $this->belongsToMany(Blog::class);
    }
}
