<?php

namespace App\Providers;

use App\Blog;
use App\Category;
use App\User;
use Illuminate\Support\ServiceProvider;

class ComposerViewProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['admin.dashboard'] , function($view) {
            $view->with([
                'blogs'=> Blog::all(),
                'categories' => Category::all(),
                'users' => User::all()
            ]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
