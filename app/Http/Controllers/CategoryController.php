<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    const IMAGES_PATH = "uploads". DIRECTORY_SEPARATOR;
    const CATEGORY_IMAGES_PATH = self::IMAGES_PATH . 'category_images' . DIRECTORY_SEPARATOR;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::latest()->get();
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get all fields from the request
        $input = $request->all();
        // Image handling
        if($request->hasFile('cat_image')){
            // get the image input from the request
            $image = $request->file('cat_image');
            // rename the image
            $image_name = uniqid() . $image->getClientOriginalName();
            // move the image to the target folder
            $image->move(public_path('/'). self::CATEGORY_IMAGES_PATH, $image_name);
            // save the image in the DB
            $input['cat_image'] = $image_name;
        }
        // create the slug
        $input['slug'] =  str_slug($request['name'], '-');

        // create other fields
        if(Category::create($input)){
            return redirect()->route('categories.index');
        } else {
            return back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $category = Category::where('slug', $slug)->first();
        return view('admin.categories.show', ['category' => $category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.categories.update', compact('category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->name = $request['name'];
        $category->slug = str_slug($request['name'], '-');
        $category->body = $request['body'];
        if($request->hasFile('cat_image')){
            // 01- get the image
            $image = $request->file('cat_image');;
            // 20- rename the image
            $cat_image_name = uniqid() . $image->getClientOriginalName();

            // 03- move the image to target folder
            $image->move(public_path('/'). self::CATEGORY_IMAGES_PATH, $cat_image_name );

            // 04- Remove the old image
            if($category->cat_image){
                $old_cat_img = public_path('/'). self::CATEGORY_IMAGES_PATH . $category->cat_image;
                if(file_exists($old_cat_img)){
                    unlink($old_cat_img);
                }
            }
            // 05 - save the image in the database
            $category->cat_image = $cat_image_name;
        }
        if($category->save()){
            return redirect()->route('categories.index');
        } else {
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        if($category->cat_image !== null ){
            $cat_image = public_path('/' . self::CATEGORY_IMAGES_PATH . $category->cat_image);
            if(file_exists($cat_image)){
                unlink($cat_image);
            }
        }
        $category->delete();
        return redirect()->route('categories.index');
    }
}
