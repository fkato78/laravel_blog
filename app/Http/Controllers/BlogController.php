<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class BlogController extends Controller
{

    /**
     * Pagination limit
     */
    const PAGE_LIMIT = 10;

    /**
     * Uploaded images folder path
     */
    const IMAGES_PATH = "uploads". DIRECTORY_SEPARATOR;
    const BLOG_IMAGES_PATH = self::IMAGES_PATH . 'featured_images' . DIRECTORY_SEPARATOR;
    const BLOG_THUMBNAILS_PATH = self::IMAGES_PATH . 'thumbnails' . DIRECTORY_SEPARATOR;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $blogs = DB::table('blogs')->orderBy('created_at', 'desc')->where('deleted_at',NULL)->paginate(10);
        $blogs = Blog::latest()->paginate(self::PAGE_LIMIT);
        return view('admin.blogs.index', compact('blogs'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();
        $tags = Tag::get();
        if($categories->count() == 0){
            Session::flash('noCategory', "U have to create category first");
            return redirect()->route('categories.create');
        }
        return view('admin.blogs.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blog = new Blog();
        $blog->title = $request->input('title');
        $blog->slug = str_slug($request->input('title'));
        $blog->body = $request->input('body');
        // SEO meta data
        $blog->meta_title = str_limit($request->input('title'), 55);
        $blog->meta_description = str_limit($request->input('body'), 155);
        // User
        $blog->user_id = Auth::user()->id;
        $roles = [
            'title' => 'required | min : 3',
            'body' => 'required',
        ];
        $this->validate($request, $roles);
        //Image handling
        if($request->hasFile('featured_image')){
            // 01- get the image form the request
            $image = $request->file('featured_image');
            // 02- rename the image
            $image_name = Carbon::now()->timestamp. '_' .$image->getClientOriginalName();
            /**
             * CREATE THUMBNAIL FROM FEATURED IMAGE via intervention package
             */
            $thumbnail = 'thumbnail_'.$image_name;
            $resizedImage = Image::make($image)->resize(100,null, function($constraint){
                $constraint->aspectRatio();
            });
            $resizedImage->save(public_path('/') . self::BLOG_THUMBNAILS_PATH . $thumbnail,100);
            $blog->thumbnail = $thumbnail;
            /**
             * Handle the image with image Intervention package OR via laravel default
             */
            // 03- upload the image
            // Via image intervention package functionality
            //Image::make($image)->save(public_path('/') . self::IMAGES_PATH . $image_name);
            // Via laravel default functionality
            $image->move(public_path('/'). self::BLOG_IMAGES_PATH, $image_name);
            // 04- save the image in the database
            $blog->featured_image = $image_name;

        }
        // Create record
        if ($blog->save()){
            /**
             * sync blog with categories and tags:: this should be after save or create method
             */
            if($request->has('category_id')){
                $blog->categories()->sync($request->category_id);
            }
            if($request->has('tag_id')){
                $blog->tags()->sync($request->tag_id);
            }
            return redirect()->route('blogs.index');
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::findOrFail($id);
        return view('admin.blogs.show')->with('blog', $blog);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allCategories = Category::latest()->get();
        $allTags = Tag::latest()->get();
        $blog = Blog::findOrFail($id);
        return view('admin.blogs.update', compact('blog', 'allCategories', 'allTags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);
        $blog->title = $request->input('title');
        $blog->body = $request->input('body');

        //Image handling
        if($request->hasFile('featured_image')){
            // 01- get the image form the request
            $image = $request->file('featured_image');
            // 02- rename the image
            $image_name = Carbon::now()->timestamp. '_' .$image->getClientOriginalName();
            /**
             * Handle the image with image Intervention package OR via laravel default
             */
            // 03- upload the image
            // Via image intervention package functionality
            //Image::make($image)->save(public_path('/') . self::IMAGES_PATH . $image_name);
            // Via laravel default functionality
            $image->move(public_path('/'). self::BLOG_IMAGES_PATH, $image_name);


            // 04- Remove the old image
            if($blog->featured_image){
                $old_post_img = public_path('/'). self::BLOG_IMAGES_PATH . $blog->featured_image;
                if(file_exists($old_post_img)){
                    unlink($old_post_img);
                }
            }
            // 05- save the new image in the database
            $blog->featured_image = $image_name;
        }

        if ($blog->save()){
            /**
             * sync blog with category and tags:: this should be after save or create method
             */
            if($request->has('category_id')){
                $blog->categories()->sync($request->category_id);
            }
            return redirect()->route('blogs.index');
        } else {
            return redirect()->back();
        }
    }

    /**
     * move posts to bin
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $blog = Blog::findOrFail($id);
        $blog->delete();
        return redirect()->route('blogs.bin');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * trash the posts
     */

    public function bin()
    {
        $deletedBlogs = Blog::onlyTrashed()->get();
        return view('admin.blogs.bin', compact('deletedBlogs'));
    }

    /**
     * restore trashed posts
     */

    public function restore($id)
    {
        $restoredBlogs = Blog::onlyTrashed()->findOrFail($id);
        $restoredBlogs->restore();
        return redirect()->route('blogs.index');

    }

    /**
     * delete the trashed posts permanently
     */

    public function removePermanent($id)
    {
        $trashedBlog = Blog::onlyTrashed()->findOrFail($id);
        // Delete post and image (if exists)
        if($trashedBlog->featured_image !== null){
            $post_image = public_path("/"). self::BLOG_IMAGES_PATH .$trashedBlog->featured_image  ;
            if(file_exists($post_image)){
                unlink($post_image);
            }
        }
        $trashedBlog->forceDelete();

        return redirect()->back();
    }

    public function publish($id)
    {
        $publishedBlog = Blog::findOrFail($id);
        if($publishedBlog->status != true){
            $publishedBlog->status = true;
            $publishedBlog->update(['status', $publishedBlog->status, 'updated_at' => Carbon::now()]);
            return back();
        } else {
            $publishedBlog->status = false;
            $publishedBlog->update(['status', $publishedBlog->status]);
            return back();
        }
    }
}
