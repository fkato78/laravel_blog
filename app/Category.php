<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'slug', 'body', 'cat_image'];

    /**
     * many to many with blog model
     */
    public function blogs()
    {
        return $this->belongsToMany(Blog::class);
    }
}
