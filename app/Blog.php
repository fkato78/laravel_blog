<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    // soft delete
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['title', 'body', 'slug', 'meta_title', 'meta_description', 'status'];

    /**
     * many to many with category model
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * many to many with tag model
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * one to many with user model
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
