<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/**
 * auth user routes
 */
Auth::routes();

/**
 * home route
 */
Route::get('/home', 'HomeController@index')->name('home');

/**
 * All admin routes
 */
Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {
    Route::get('/', 'AdminController@index')->name('admin');
    // Blog model routes
    Route::group(['prefix' => 'blogs' , 'middleware' => 'auth'], function () {
    //  Soft delete future
        Route::get('/bin', 'BlogController@bin')->name('blogs.bin');
        Route::get('/bin/{id}/restore', 'BlogController@restore');
        Route::delete('/bin/{id}/remove', 'BlogController@removePermanent');

        Route::get('/', 'BlogController@index')->name('blogs.index');
        Route::get('create', 'BlogController@create')->name('blogs.create');
        Route::post('store', 'BlogController@store');
        Route::get('{id}', 'BlogController@show')->name('blogs.show');
        Route::get('{id}/edit', 'BlogController@edit')->name('blogs.update');
        Route::patch('{id}', 'BlogController@update');
        Route::delete('{id}', 'BlogController@destroy');
        Route::get('{id}/change_status','BlogController@publish' )->name('blogs.change_status');
    });
    //  category route resource
    Route::resource('categories', 'CategoryController')->middleware(['auth']);

    //  tag route resource
    Route::resource('tags', 'TagController')->middleware(['auth']);
});


//Route::resource('blogs', 'BlogController');